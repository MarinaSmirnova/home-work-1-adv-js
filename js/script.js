class Employee {
   constructor (name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
   }

   get name() {
      return this._name;
   }

   get age() {
      return this._age;
   }

   get salary() {
      return this._salary;
   }

   set name(newName) {
      this._name = newName;
    }

   set age(newAge) {
      (newAge > 0)&&(typeof newAge === "number") ? this._age = newAge : console.log("Please enter a number greater than 0.");
   }

   set salary(newSalary) {
      (newSalary > 0)&&(typeof newSalary === "number") > 0 ? this._salary = newSalary : console.log("Please enter a number greater than 0.");
   }
}

class Programmer extends Employee {
   constructor (name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
   }
   get salary() {
      return super.salary *3;
   }
}

const marina = new Programmer("Marina", 27, 10000000, "JS");
const anna = new Programmer("Anna", 28, 120000, "C, C++");
const dmytro = new Programmer("Dmytro", 34, 500000, "PHP, Python");

console.log(marina);
console.log(anna);
console.log(dmytro);

